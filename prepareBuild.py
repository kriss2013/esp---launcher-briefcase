# adapted from https://byte-style.de/2018/01/automatische-updates-fuer-microcontroller-mit-gitlab-und-platformio/
Import("env")
import os
import git
import time

#import sys
#print sys.executable
#print '\n'.join(sys.path)

BUILD_DIR = env['PROJECT_BUILD_DIR']+'/'+env['PIOENV']
SOURCE_FILE_NAME = "main.cpp" # should be calcuated automatically, I don't know yet how to do it

VERSION_FILE = "version.h" #this file should be generated (empty) once manually in BUILD_DIR


BUILDNR = int(round(time.time()))
GITVERSION = ""
BOARD = env['BOARD']


cwd = os.path.abspath(os.getcwd())
r = git.repo.Repo(cwd)
GITVERSION = r.git.describe('--always'); # the '--always' option was necessary for me, otherwise I get an error

def in_docker():
    """ Returns: True if running in a docker container, else False """
    try:
        with open('/proc/1/cgroup', 'rt') as ifh:
            return 'docker' in ifh.read()
    except:
        return False

print "----------------------- BUILD STARTED -----------------------"
print "Info:"
print "-------------------------------------------------------------"
if in_docker():
    print "CI-Process, generate build version"
    print 'Docker container: No deletion of build files'
else:
    print "No CI-Process, generate dev version"
    GITVERSION = GITVERSION + "_dev"
print "Current build targets: \t", map(str, BUILD_TARGETS)
print "Current build dir: \t"+BUILD_DIR
print "Board: \t\t"+env['BOARD']
#print "Progname: \t"+env['PROGNAME']
print "Version: \t"+GITVERSION
print "Build Nr: \t"+str(BUILDNR)
print "In Docker: \t"+str(in_docker())
print "-------------------------------------------------------------"


def prepareVersion(source, target, env):
    print "----------------------- prepareVersionAndConfig Start -----------------------"
    
    FILE = os.path.join(os.path.join(cwd, "src"), VERSION_FILE)
    print FILE
    os.remove(FILE)
    if os.path.exists(FILE):
        f = file(FILE, "r+")
    else:
        f = file(FILE, "w")
    f.write('const int FW_VERSION = '+str(BUILDNR)+';\n#ifndef VERSION_H\n#define VERSION_H\n#define _VER_ ( "'+GITVERSION+'" )\n#endif //VERSION_H')
    f.close()
    
    print "----------------------- prepareVersionAndConfig End -----------------------"


# an env.AddPreAction with a target trigger like 'buildprog' will be executed too late in the process. Hnce the .o file as trigger
env.AddPreAction(BUILD_DIR+"/src/"+SOURCE_FILE_NAME+".o",prepareVersion)