# ESP - Launcher-Briefcase
## Description
a "big red button" embeded into a military looking rocket launch suitcase
## Functionalities
on opening the case (opening still unprotected) the folloing workflow is expexcted,  each steps requires some kind of secret access procedure:
1. power on the case with a key. The key must first be found
2. connect a browser enabled device, like a smartphone, to the given IP Address, thie requires the WLAN password
3. connect to the webserver of the suitcase and launch the program. This step is not yet protected.
4. switch the 3 toggle switches in the right order at the right time, all 3 timers duration are dynamically defined when the web site is visited. To perform this step succesfully one must know which is swith 1, which is switch 2 etc. Obviously it is NOT the physical order ;-)
5. if step 4 was sucessfull, the web site activetes the "big red button" for 30 seconds - boom!
## Architecture
### Hardware
- [Military Suitcase](https://www.amazon.de/gp/product/B00E4HNE1M)
- [WeMos D1](https://de.aliexpress.com/item/32651747570.html)
- [pull up resistor](https://de.aliexpress.com/item/32636020144.html)
- [perfboard](https://de.aliexpress.com/item/4000070053237.html)
- [key switch](https://www.amazon.de/gp/product/B008X0ZNUG)
- [protected toggle switch](https://www.amazon.de/gp/product/B07H2RW24C)
- [BRB (Big Red Button)](https://www.amazon.de/gp/product/B071X5QMTC)
- D1 mini GPIO mapping
  - Indicator D4
  - ToggleSwitch1 D5
  - ToggleSwitch2 D6
  - ToggleSwitch3 D7
  - ToggleSwitchGround D8
  - BigRedButton D1
  - BigRedButtonGround D2
### Software
- on power on the suitcase tries to connect to a WiFi network. It asks for credentials on first connect and saves it for subsequent use
- then it spawns a web site
- the 3 timers are defined when the web site is visited
- upon succesfull completion of the manual steps describes above, the case post an MQTT message to a secret broker
- any services which subscribed to the message queue can do wahtever they want with it. This includes potentially launching a nuclear rocket;-)