#include <Arduino.h>

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <PubSubClient.h>
#include "version.h"


#include "index.h" //Our HTML webpage contents

#define HOSTNAME "IoT-ESP-Launcher"

const char* mqtt_server = "94.130.74.255";

#define CAT(x, y) CAT_(x, y)
#define CAT_(x, y) x ## y

#define Indicator D4
#define SwitchIndicatorOn digitalWrite(Indicator,0)
#define SwitchIndicatorOff digitalWrite(Indicator,1)

#define ToggleSwitch1 D5
#define ToggleSwitch2 D6
#define ToggleSwitch3 D7
#define ToggleSwitchGround D8

static uint8_t ToggleSwitch[3] = {
  ToggleSwitch1,
  ToggleSwitch2,
  ToggleSwitch3
};

#define ToggleSwitchState(IDX) digitalRead(ToggleSwitch[IDX])

#define BigRedButton D1
#define BigRedButtonGround D2
#define BigRedButtonState digitalRead(BigRedButton)

#define SerialPrintDefconState   Serial.print("Defcon State: "); Serial.println(defconState)


ESP8266WebServer server(80);

void configModeCallback (WiFiManager *myWiFiManager);

WiFiClient espClient;
PubSubClient client(espClient);

uint8_t timer1,timer2,timer3;
uint8_t permutationIndex;

static uint8_t permutations[6][3] = {
  {1,2,3},
  {1,3,2},
  {2,1,3},
  {2,3,1},
  {3,1,2},
  {3,2,1}
};

unsigned long stopTime;
bool timerRuns=false;

bool rocketLaunched=false;

// server code from https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WebServer/examples/HelloServer/HelloServer.ino 
void handleRoot() {
  SwitchIndicatorOn;
  
  Serial.println("Launch sequence started");

  String htmlTemplate = MAIN_page;
  String htmlCode = htmlTemplate;

  String timer1Placeholder = "TIMER1";
  String timer2Placeholder = "TIMER2";
  String timer3Placeholder = "TIMER3";

  timer1=random(20,120);
  timer2=random(20,120);
  timer3=random(20,120);
  String timer1Value = String(timer1);
  String timer2Value = String(timer2);
  String timer3Value = String(timer3);

  htmlCode.replace(timer1Placeholder,timer1Value);
  htmlCode.replace(timer2Placeholder,timer2Value);
  htmlCode.replace(timer3Placeholder,timer3Value);

  String toggleSwitch1Placeholder = "KS1";
  String toggleSwitch2Placeholder = "KS2";
  String toggleSwitch3Placeholder = "KS3";

  permutationIndex = random(0,5);
  String toggleSwitch1Value = String(permutations[permutationIndex][0]);
  String toggleSwitch2Value = String(permutations[permutationIndex][1]);
  String toggleSwitch3Value = String(permutations[permutationIndex][2]);

  htmlCode.replace(toggleSwitch1Placeholder,toggleSwitch1Value);
  htmlCode.replace(toggleSwitch2Placeholder,toggleSwitch2Value);
  htmlCode.replace(toggleSwitch3Placeholder,toggleSwitch3Value);
 
  server.send(200,"text/html",htmlCode);
  
  Serial.println("Permutations:");
  for (int idx=0; idx<6;idx++) {
    Serial.print(permutations[idx][0]);
    Serial.print(permutations[idx][1]);
    Serial.println(permutations[idx][2]);
  }

  Serial.print("Permutation index: ");Serial.println(permutationIndex);
  Serial.print(permutations[permutationIndex][0]);
  Serial.print(permutations[permutationIndex][1]);
  Serial.println(permutations[permutationIndex][2]);

  stopTime=millis()+105*timer1;
  timerRuns=true;
  Serial.print("Timer started for ");Serial.print(105*timer1);Serial.println(" ms");

  client.setServer(mqtt_server, 1883);
  
  SwitchIndicatorOff;
}

void handleNotFound() {
  digitalWrite(D4, 0);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(D4, 1);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (unsigned int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "Launcher";
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      //client.publish("outTopic", "hello world");
      // ... and resubscribe
      //client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

uint8_t defconState=0;

void setup() {

  digitalWrite(ToggleSwitchGround,0);
  pinMode(ToggleSwitchGround,OUTPUT);

  digitalWrite(BigRedButtonGround,0);
  pinMode(BigRedButtonGround,OUTPUT);

  SwitchIndicatorOn;
  pinMode(Indicator, OUTPUT);


  pinMode(ToggleSwitch1,INPUT);
  pinMode(ToggleSwitch2,INPUT);
  pinMode(ToggleSwitch3,INPUT);

  pinMode(BigRedButton,INPUT);


  Serial.begin(9600);

  // put your setup code here, to run once:

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  // Uncomment for testing wifi manager
  //wifiManager.resetSettings();
  wifiManager.setAPCallback(configModeCallback);

  //or use this for auto generated name ESP + ChipID
  wifiManager.autoConnect();

  server.on("/", handleRoot);

  server.begin();

  Serial.println("HTTP server started");

  Serial.println("toggle switch physical mapping:");
  Serial.print("Switch 1: ");Serial.println(ToggleSwitch[0]);
  Serial.print("Switch 2: ");Serial.println(ToggleSwitch[1]);
  Serial.print("Switch 3: ");Serial.println(ToggleSwitch[2]);

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  Serial.println(WiFi.macAddress());


  SerialPrintDefconState;

  SwitchIndicatorOff;

}


void loop() 
{
  server.handleClient();

   if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (3==defconState && 0==BigRedButtonState && false == rocketLaunched) {
    rocketLaunched=true;
    //char msg[50];
    //snprintf (msg, 50, "hello world #%ld", value);
    client.publish("ccl/work/launcher", "go");
    Serial.println("Rocket launched!");
    for(int idx=0;idx<30;idx++){
      SwitchIndicatorOn;
      delay(100);
      SwitchIndicatorOff;
      delay(50);     
    }
  }

  if (true == timerRuns && millis() > stopTime) 
  {
    Serial.println("Timer fired");
    timerRuns=false;

    Serial.println("Physical State of toggle switch:");
    Serial.println("123R");
    Serial.print(ToggleSwitchState(0));
    Serial.print(ToggleSwitchState(1));
    Serial.print(ToggleSwitchState(2));
    Serial.println(BigRedButtonState);

    Serial.println("Permutated State of toggle switch:");
    Serial.print(permutations[permutationIndex][0]);
    Serial.print(permutations[permutationIndex][1]);
    Serial.println(permutations[permutationIndex][2]);
    Serial.print(ToggleSwitchState(permutations[permutationIndex][0]-1));
    Serial.print(ToggleSwitchState(permutations[permutationIndex][1]-1));
    Serial.println(ToggleSwitchState(permutations[permutationIndex][2]-1));
        

    switch (defconState) 
    {
      case 0:
        if (
          0==ToggleSwitchState(permutations[permutationIndex][0]-1) &&
          1==ToggleSwitchState(permutations[permutationIndex][1]-1) &&
          1==ToggleSwitchState(permutations[permutationIndex][2]-1) 
          ) 
        {
          stopTime=millis()+105*timer2;
          timerRuns=true;
          Serial.print("Timer started for ");Serial.print(105*timer2);Serial.println(" ms");
          defconState++;
        } 
        else 
        {
          defconState=0;
        }
      break;
      case 1:
        if (
          0==ToggleSwitchState(permutations[permutationIndex][1]-1) &&
          1==ToggleSwitchState(permutations[permutationIndex][2]-1) 
          ) 
        {
          stopTime=millis()+105*timer3;
          timerRuns=true;
          Serial.print("Timer started for ");Serial.print(105*timer3);Serial.println(" ms");
          defconState++;
        }
        else 
        {
          defconState=0;
        }
      break;
      case 2:
        if (0==ToggleSwitchState(permutations[permutationIndex][2]-1)) 
        {
          defconState++;
        }
        else 
        {
          defconState=0;
        }
      break;
      // default:
    }
    
    SerialPrintDefconState;
  }

}


void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
}

