// from https://circuits4you.com/2016/12/16/esp8266-web-server-html/
// string literal -> https://en.cppreference.com/w/cpp/language/string_literal
const char MAIN_page[]  = R"=====(
<!DOCTYPE html>
<html>
<style>
#myProgress1 {
  width: 100%;
  margin-top:5px;
  margin-bottom: 5px;
  background-color: #ddd;
}

#myBar1 {
  width: 0%;
  height: 40px;
  background-color: #4CAF50;
}

#myProgress2 {
  width: 100%;
  margin-top:5px;
  margin-bottom: 5px;
  background-color: #ddd;
}

#myBar2 {
  width: 0%;
  height: 40px;
  background-color: #4CAF50;
}

#myProgress3 {
  width: 100%;
  margin-top:5px;
  margin-bottom: 5px;
  background-color: #ddd;
}

#myBar3 {
  width: 0%;
  height: 40px;
  background-color: #4CAF50;
}

.inner {
  margin-left: 10px;
  color: white;
}

</style>
<body>

<h1>Kippschalter Aktivierung</h1>

<div id="myProgress1">
  <div id="myBar1">
  	<div class="inner"><h1>_</h1></div>
  </div>	
</div>

<div id="myProgress2">
  <div id="myBar2">
  	<div class="inner"><h1>_</h1></div>
  </div>
</div>

<div id="myProgress3">
  <div id="myBar3">
  	<div class="inner"><h1>_</h1></div>
  </div>
</div>

<script>

setTimeout(function() {
  move1();
}, 1000);

function move1() {
  var elem = document.getElementById("myBar1");   
  var width = 0;
  var id1 = setInterval(frame1, TIMER1);
  elem.innerHTML="<div class=\"inner\"><h1>KS1</h1></div>";
  function frame1() {
    if (width >= 100) {
      clearInterval(id1);
      move2();
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}

function move2() {
  var elem = document.getElementById("myBar2");   
  var width = 0;
  var id2 = setInterval(frame2, TIMER2);
  elem.innerHTML="<div class=\"inner\"><h1>KS2</h1></div>";
  function frame2() {
    if (width >= 100) {
      clearInterval(id2);
      move3();
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}

function move3() {
  var elem = document.getElementById("myBar3");   
  var width = 0;
  var id3 = setInterval(frame3, TIMER3);
  elem.innerHTML="<div class=\"inner\"><h1>KS3</h1></div>";
  function frame3() {
    if (width >= 100) {
      clearInterval(id3);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}

</script>

</body>
</html>
)=====";